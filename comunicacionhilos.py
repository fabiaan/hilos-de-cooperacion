from threading import *
import time


#Comunicacion entre subprocesos con python mediante eventos

#metodo para las acciones del policia de trafico
def traffic_police():
    
   while True:  #Mientras sea cierto:
       time.sleep(5) #pone en espera de 5 seg el hilo actual
       
       #2. El policia da la señal en verde
       print("Policía de Tránsito dando señal VERDE") #Se lanza un mensaje 
       
       #llamamos al event.set, haciendo que el indicador interno de la clase event 
       #3. pase de false a true.
       event.set()
       time.sleep(10)
       
       #5. Como pasaron los 10 segundos, volvemos al metodo del policia
       #y hacemos saber que esta dando la señal en verde
       print("Policía de Tránsito Dando Señal ROJA")
       
       # 6. Se pone nuevamente el evento en false mediante event.clear
       event.clear()

def driver():
    
    #Definimos una variable numerica
   num=0
   #mientras sea cierto...
   while True:
       
       #1. Los conductores estan esperando la señal en verde
       print("Conductores esperando la señal VERDE")
       
       #Con el event.wait, hacemos que el subproceso espere al evento, el cual se encuentra
       #en false, bloqueando asi el subproceso hasta que el evento sea true mediante el set,
       #esto pasa en la funcion del policia, cuando se usa el event.set, su valor cambia a true
       #y entonces es cuando se ejecuta el hilo moestrando que los vehiculos pueden moverse
       event.wait()
             
       
       print("El semáforo esta VERDE... Los vehículos pueden moverse")
       
       
       #4. Mientras el evento esté en verdadero, los vehiculos pueden avanzar
       while event.is_set():
        
           #aumentamos al contador
           num=num+1
           
           #imprimimos el numero del vehiculo que esta pasando en ese momento el semaforo
           print("Vehiculo No:", num,"Cruzando la señal")
           #damos un timer de 2 segundos
           time.sleep(2)
        #Y ponemos nuevamente la señal en rojo.
       print("El semáforo está en ROJO... Los conductores tienen que esperar")

#1. Definimos un nuevo evento: se define inicialmente como false al ser un evento "sin proposito aun"
event=Event()

#Se crean 2 hilos, uno para el policia de trafico y otro para el conductor
t1=Thread(target=traffic_police)
t2=Thread(target=driver)

#se inician ambos hilos
t1.start()
t2.start()


"""
Conductores esperando la señal VERDE
Policía de Tránsito dando señal VERDE
El semáforo esta VERDE... Los vehículos pueden moverse
Vehiculo No: 6 Cruzando la señal
Vehiculo No: 7 Cruzando la señal
Vehiculo No: 8 Cruzando la señal
Vehiculo No: 9 Cruzando la señal
Vehiculo No: 10 Cruzando la señal
Policía de Tránsito Dando Señal ROJA
El semáforo está en ROJO... Los conductores tienen que esperar

"""