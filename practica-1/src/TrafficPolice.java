// Clase que extiende de Thread para crear un hilo
public class TrafficPolice extends Thread {
  
  // Sobreescribimos el método RUN de Thread
  @Override
  // Retornamos el método run para que se pueda sincronizar con otros hilos
  public synchronized void run(){
    try {
      while(true){
        // Suspendemos este hilo por 5 segundos
        Thread.sleep(5000);

        System.out.println("Policía de Tránsito dando señal VERDE");

        // Suspendemos el hilo 10 segundos
        Thread.sleep(10000);
        
        // Esperamos la notificación del otro hilo mediante el método wait()
        notify();

        System.out.println("Policía de Tránsito Dando Señal ROJA");

        // Suspendemos este hilo 5 segundos
        wait(5000);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
