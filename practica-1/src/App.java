public class App {
  public static void main(String[] args) throws InterruptedException {

    // Creamos los dos hilos
    TrafficPolice trafficPolice = new TrafficPolice();
    Driver driver = new Driver();

    // Iniciamos los dos hilos
    trafficPolice.start();
    driver.start();

    // Hacemos que estos hilos puedes esperar entre sí
    trafficPolice.join();
    driver.join();
  }
}