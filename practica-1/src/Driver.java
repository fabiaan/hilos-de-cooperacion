// Clase que extiende de Thread para crear un hilo
public class Driver extends Thread {
  
  // Sobreescribimos el método RUN de Thread
  @Override
  // Retornamos el método run para que se pueda sincronizar con otros hilos
  public synchronized void run(){
    try {
      int num = 0;
      int aux = 5;

      while (true) {
        // Al estar suspendido el otro hilo, este inicia primero
        System.out.println("Conductores esperando la señal VERDE");

        // Suspendemos este hilo 5 segundos
        wait(5000);

        System.out.println("El semáforo está en VERDE... Los vehículos pueden moverse");
  
        while (num < aux) {
          num = num + 1;

          System.out.println("Vehiculo No:"+ num +" - Cruzando la señal");

          // Cada vez que pasa un vehículo el hilo se suspende 2 segundos
          Thread.sleep(2000);
        }
        aux = num + 5;
        System.out.println("El semáforo está en ROJO... Los conductores tienen que esperar");

        // Suspendemos este hilo 5 segundos
        wait(5000);
      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}